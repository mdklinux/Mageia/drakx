package harddrake::sound;

# TODO: 
#    o ensure sound is not user (either dsp/midi/sequencer/mixer)
#    o fix sound/alsa services

use strict;
use common;
use run_program;
use modules;
use list_modules;
use detect_devices;
use log;

sub is_pulseaudio_enabled {
    my ($in) = @_;
    my $soundprofile = common::read_alternative('soundprofile');
    return $in->do_pkgs->is_installed('task-pulseaudio') && $soundprofile =~ /pulse$/;
}

sub set_pulseaudio {
    my ($val) = @_;

    my $alterative = '/etc/sound/profiles/' . ($val ? 'pulse' : 'alsa');
    return if ! -d $alterative;

    common::symlinkf_update_alternatives('soundprofile', $alterative);

    # (cg) This config file will eventually be dropped, but it is still needed for now
    # as several packages/patches depend on it.
    my $config_file = "$::prefix/etc/sysconfig/pulseaudio";
    $val = 'PULSE_SERVER_TYPE=' . ($val ? 'personal' : 'none') . "\n";
    my $done;
    substInFile {
        if (/^PULSE_SERVER_TYPE=/) {
            $_ = $val;
            $done = 1;
        }
    } $config_file;
    append_to_file($config_file, $val) if !$done;
}


my $pa_startup_scriptfile = "$::prefix/etc/pulse/default.pa";

sub is_pulseaudio_glitchfree_enabled() {
    return -f $pa_startup_scriptfile &&
      cat_($pa_startup_scriptfile) !~ /^load-module\s+module-(udev|hal)-detect\s+tsched=0/m;
}

sub set_pulseaudio_glitchfree {
    my ($val) = @_;

    return if ! -f $pa_startup_scriptfile;

    substInFile {
        if ($val) {
            s/^(load-module\s+module-(udev|hal)-detect)\s+tsched=0/$1/;
        } else {
            s/^(load-module\s+module-(udev|hal)-detect).*/$1 tsched=0/;
        }
    } $pa_startup_scriptfile;
}

sub is_pipewire_wireplumber_enabled {
    my ($in) = @_;
    return $in->do_pkgs->is_installed('pipewire') && $in->do_pkgs->is_installed('wireplumber');
}

sub is_pipewire_media_session_enabled {
    my ($in) = @_;
    return $in->do_pkgs->is_installed('pipewire') && $in->do_pkgs->is_installed('pipewire-media-session');
}

sub rooted {
    run_program::rooted($::prefix, @_);
}

sub unload { modules::unload(@_) if $::isStandalone }

sub load { 
    my ($modules_conf, $name) = @_;
    modules::load_and_configure($modules_conf, $name) if $::isStandalone;
}

sub _pidof {
    my ($name) = @_;
    rooted('/usr/bin/pidof', $name);
}

# return the pid of a running command
sub _pidof_pid {
    my ($name) = @_;
    my ($pid) = chomp_(run_program::rooted_get_stdout($::prefix, '/usr/bin/pidof', $name));

    return $pid;
}

# return the username of a running command
sub _user_pid {
    my ($name) = @_;
    my ($pid) = _pidof_pid($name);
    my $user;

    if ($pid) {
        ($user) = chomp_(run_program::rooted_get_stdout($::prefix, '/usr/bin/ps', '-o', 'uname=', '-p', $pid));
    }

    return $user;
}

# stop pulseaudio for the running user
sub stop_pulseaudio {
    my ($pulseaudio_user) = _user_pid('/usr/bin/pulseaudio');

    if ($pulseaudio_user) {
        if (-x $::prefix . '/usr/bin/pulseaudio') {
            rooted('/usr/bin/su', '-l', $pulseaudio_user, '-c', '/usr/bin/pulseaudio --kill');
        }
    }

    # try stopping again (in case the /usr/bin/pulseaudio process is still running, but the executable no longer available)
    my ($pulseaudio_user) = _user_pid('/usr/bin/pulseaudio');
    if ($pulseaudio_user) {
        my ($pulseaudio_pid) = _pidof_pid('/usr/bin/pulseaudio');
        if ($pulseaudio_pid) {
            rooted('/usr/bin/su', '-l', $pulseaudio_user, '-c', '/usr/bin/kill -TERM ' . $pulseaudio_pid);
        }
    }
    return $pulseaudio_user;
}

# stop pipewire services for the running user
sub stop_pipewire {
    my ($pipewire_user) = _user_pid('/usr/bin/pipewire');

    if ($pipewire_user) {
        rooted('/usr/bin/systemctl', '--machine=' . $pipewire_user . '@.host', '--user', 'stop', 'pipewire-pulse.service', 'pipewire-pulse.socket', 'pipewire.service', 'pipewire.socket');
    }

    return $pipewire_user;
}

# start pulseaudio for the specified user
sub start_pulseaudio {
    my ($pulseaudio_user) = @_;

    if ($pulseaudio_user) {
        rooted('/usr/bin/su', '-l', $pulseaudio_user, '-c', '/usr/bin/pulseaudio --start');
    }
}

# start pipewire services for the specified user
sub start_pipewire {
    my ($pipewire_user) = @_;

    if ($pipewire_user) {
        rooted('/usr/bin/systemctl', '--machine=' . $pipewire_user . '@.host', '--user', 'start', 'pipewire.socket', 'pipewire.service', 'pipewire-pulse.socket', 'pipewire-pulse.service');
    }
}

# stop wireplumber services for the running user
sub stop_wireplumber {
    my ($pipewire_user) = _user_pid('/usr/bin/wireplumber');

    if ($pipewire_user) {
        rooted('/usr/bin/systemctl', '--machine=' . $pipewire_user . '@.host', '--user', 'stop', 'wireplumber.service');
    }

    return $pipewire_user;
}

# start wireplumber
sub start_wireplumber {
    my ($pipewire_user) = @_;

    if ($pipewire_user) {
         rooted('/usr/bin/systemctl', '--machine=' . $pipewire_user . '@.host', '--user', 'start', 'wireplumber.service');
    }
}

# stop pipewire-media-session services for the running user
sub stop_pipewire_media_session {
    my ($pipewire_user) = _user_pid('/usr/bin/pipewire-media-session');

    if ($pipewire_user) {
        rooted('/usr/bin/systemctl', '--machine=' . $pipewire_user . '@.host', '--user', 'stop', 'pipewire-media-session.service');
    }

    return $pipewire_user;
}

# start pipewire-media-session services for the specified user
sub start_pipewire_media_session {
    my ($pipewire_user) = @_;

    if ($pipewire_user) {
        rooted('/usr/bin/systemctl', '--machine=' . $pipewire_user . '@.host', '--user', 'start', 'pipewire-media-session.service');
    }
}

sub configure_pipewire_wireplumber {
    my ($in) = @_;
    my ($pipewire_wp_user);

    # preserve pulseaudio user
    my ($pulseaudio_user) = _user_pid('/usr/bin/pulseaudio');

    # stop pipewire and pipewire-media-session services (if any) before removing any packages
    # and preserve pipewire-media-session user
    $pipewire_wp_user = stop_pipewire_media_session();
    stop_pipewire();

    my $plasma_installed = $in->do_pkgs->is_installed('plasma-desktop');
    my @pkgs = (
        'task-pipewire',
        'wireplumber',
    );
    if (!$in->do_pkgs->is_installed('pavucontrol-qt')) {
        if ($plasma_installed) {
            push(@pkgs, 'pavucontrol-qt');
        }
        else {
            push(@pkgs, 'pavucontrol');
        }
    }


    my $required_installed = $in->do_pkgs->ensure_are_installed(
        \@pkgs
    );

    if (!$required_installed)
    {
        $in->ask_warn(N("Couldn't install the required packages"),
                      N("Please check the repositories are correctly configured")
        );
        return;
    }

    set_pulseaudio(0);

    # first of all disabling what has to be disabled to avoid conflicts
    if ($in->do_pkgs->is_installed('pipewire-media-session')) {
        rooted('/usr/bin/systemctl', '--global', 'disable', 'pipewire-media-session.service');
    }

    if ($in->do_pkgs->is_installed('pipewire')) {
        foreach ('pipewire.socket', 'pipewire.service') {
            rooted('/usr/bin/systemctl', '--global', 'enable', $_);
        }
    }

    if ($in->do_pkgs->is_installed('pipewire-pulseaudio')) {
        foreach ('pipewire-pulse.socket', 'pipewire-pulse.service') {
            rooted('/usr/bin/systemctl', '--global', 'enable', $_);
        }
    }

    if ($in->do_pkgs->is_installed('wireplumber')) {
        rooted('/usr/bin/systemctl', '--global', 'enable', 'wireplumber.service');
    }


    # stop pulseaudio process if still floating after the switch
    stop_pulseaudio();

    if ($pulseaudio_user) {
    # restart pipewire and wireplumber with the same user as initial pulseaudio
        start_pipewire($pulseaudio_user);
        start_wireplumber($pulseaudio_user);
    }
    else {
        if ($pipewire_wp_user) {
        # restart pipewire and wireplumber with the same user as pipewire-media-session
            start_pipewire($pipewire_wp_user);
            start_wireplumber($pipewire_wp_user);
        }
    }

    #Plasma tricks
    if ($plasma_installed) {
        $in->do_pkgs->ensure_is_installed('kpipewire');
    }
}

sub configure_pipewire_media_session {
    my ($in) = @_;
    my ($pipewire_ms_user);

    # preserve pulseaudio user
    my ($pulseaudio_user) = _user_pid('/usr/bin/pulseaudio');

    # stop pipewire and wireplumber services (if any) before removing any package
    # and preserve wireplumber user
    $pipewire_ms_user = stop_wireplumber();
    stop_pipewire();


    my $plasma_installed = $in->do_pkgs->is_installed('plasma-desktop');
    my @pkgs = (
        'task-pipewire',
        'pipewire-media-session',
    );
    if (!$in->do_pkgs->is_installed('pavucontrol-qt')) {
        if ($plasma_installed) {
            push(@pkgs, 'pavucontrol-qt');
        }
        else {
            push(@pkgs, 'pavucontrol');
        }
    }

    my $required_installed = $in->do_pkgs->ensure_are_installed(
        \@pkgs
    );

    if (!$required_installed)
    {
        $in->ask_warn(N("Couldn't install the required packages"),
                      N("Please check the repositories are correctly configured")
        );
        return;
    }

    set_pulseaudio(0);

    # first of all disabling what has to be disabled to avoid conflicts
    if ($in->do_pkgs->is_installed('wireplumber')) {
        rooted('/usr/bin/systemctl', '--global', 'disable', 'wireplumber.service');
    }

    if ($in->do_pkgs->is_installed('pipewire')) {
        foreach ('pipewire.socket', 'pipewire.service') {
            rooted('/usr/bin/systemctl', '--global', 'enable', $_);
        }
    }

    if ($in->do_pkgs->is_installed('pipewire-pulseaudio')) {
        foreach ('pipewire-pulse.socket', 'pipewire-pulse.service') {
            rooted('/usr/bin/systemctl', '--global', 'enable', $_);
        }
    }

    if ($in->do_pkgs->is_installed('pipewire-media-session')) {
        rooted('/usr/bin/systemctl', '--global', 'enable', 'pipewire-media-session.service');
    }

    # stop pulseaudio process if still floating after the switch
    stop_pulseaudio();

    if ($pulseaudio_user) {
    # restart pipewire and wireplumber with the same user as initial pulseaudio
        start_pipewire($pulseaudio_user);
        start_pipewire_media_session($pulseaudio_user);
    }
    else {
        if ($pipewire_ms_user) {
        # restart pipewire and wireplumber with the same user as pipewire-media-session
            start_pipewire($pipewire_ms_user);
            start_pipewire_media_session($pipewire_ms_user);
        }
    }

    #Plasma tricks
    if ($plasma_installed) {
        $in->do_pkgs->ensure_is_installed('kpipewire');
    }
}

sub disable_all_pipewire {
    my ($in) = @_;

    if ($in->do_pkgs->is_installed('wireplumber')) {
        rooted('/usr/bin/systemctl', '--global', 'disable', 'wireplumber.service');
    }

    if ($in->do_pkgs->is_installed('pipewire-media-session')) {
        rooted('/usr/bin/systemctl', '--global', 'disable', 'pipewire-media-session.service');
    }

    if ($in->do_pkgs->is_installed('pipewire-pulseaudio')) {
        foreach ('pipewire-pulse.socket', 'pipewire-pulse.service') {
            rooted('/usr/bin/systemctl', '--global', 'disable', $_);
        }
    }

    if ($in->do_pkgs->is_installed('pipewire')) {
        foreach ('pipewire.socket', 'pipewire.service') {
            rooted('/usr/bin/systemctl', '--global', 'disable', $_);
        }
    }
}

sub configure_pulseaudio {
    my ($in) = @_;

    # preserve pipewire running user
    my ($pipewire_user) = _user_pid('/usr/bin/pipewire');

    # stop pipewire, wireplumber and pipewire-media-session services (if any) before removing any packages
    my ($wireplumber_user) = stop_wireplumber();
    my ($pipewire_media_session_user) = stop_pipewire_media_session();
    my ($pipewire_user_preserve) = stop_pipewire();

    # now packages
    $in->do_pkgs->remove('pipewire-alsa');

    my $plasma_installed = $in->do_pkgs->is_installed('plasma-desktop');
    my @pkgs = 'task-pulseaudio';
    if (!$in->do_pkgs->is_installed('pavucontrol-qt')) {
        if ($plasma_installed) {
            push(@pkgs, 'pavucontrol-qt');
        }
        else {
            push(@pkgs, 'pavucontrol');
        }
    }

    my $required_installed = $in->do_pkgs->ensure_are_installed(
        \@pkgs
    );

    if (!$required_installed)
    {
        $in->ask_warn(N("Couldn't install the required packages"),
                      N("Please check the repositories are correctly configured")
        );

        # restore previous pipewire (if any) processes if something goes wrong with pulseaudio installation
        if ($pipewire_user_preserve) {
            start_pipewire($pipewire_user_preserve);
        }

        if ($wireplumber_user) {
            start_wireplumber($wireplumber_user);
        }

        if ($pipewire_media_session_user) {
            start_pipewire_media_session($pipewire_media_session_user);
        }

        return;
    }


    # disable all the pipewire stuff before managing packages
    disable_all_pipewire($in);

    # start pulseaudio with same pipewire preserved user
    if ($pipewire_user) {
        start_pulseaudio($pipewire_user);
    }
}

sub config {
    my ($in, $modules_conf, $device) = @_;
    my $driver = $device->{current_driver} || $device->{driver};

    my @alternative = $driver ne $device->{driver} ? $device->{driver} : ();
    if ($driver eq "unknown") {
        $in->ask_warn(N("No known driver"),
                      N("There's no known driver for your sound card (%s)",
                        $device->{description}));
    } else {
        push @alternative, $driver;
        my %des = modules::category2modules_and_description('multimedia/sound');
        
        my $is_pulseaudio_enabled_val = is_pulseaudio_enabled($in);
        my $is_pulseaudio_glitchfree_enabled_val = is_pulseaudio_glitchfree_enabled();

        my $audiosystem = 'None';
        if ($is_pulseaudio_enabled_val && $is_pulseaudio_glitchfree_enabled_val) {
            $audiosystem = 'PulseAudioGF';
        } elsif ($is_pulseaudio_enabled_val) {
            $audiosystem = 'PulseAudio';
        } elsif (is_pipewire_wireplumber_enabled($in) || _pidof('/usr/bin/wireplumber')) {
            $audiosystem = 'PipeWireWP';
        } elsif (is_pipewire_media_session_enabled($in) || _pidof('/usr/bin/pipewire-media-session')) {
            $audiosystem = 'PipeWireMS';
        }

        my $write_config = sub {
            if ($audiosystem eq 'None') {
            #### ALSA alone
                disable_all_pipewire($in);
                set_pulseaudio(0);
                set_pulseaudio_glitchfree(0);
                # TODO check if adding autospawn = no to /etc/pulse/client.conf is needed
            } elsif ($audiosystem eq 'PulseAudio') {
            #### PulseAudio
                configure_pulseaudio($in);
                set_pulseaudio(1);
                set_pulseaudio_glitchfree(0);
                my $lib = get_libdir();
                $in->do_pkgs->ensure_is_installed($lib . 'alsa-plugins-pulseaudio',
                                                  '/usr/' . $lib . '/alsa-lib/libasound_module_pcm_pulse.so');
            } elsif ($audiosystem eq 'PulseAudioGF') {
            #### PulseAudio with Glitch-Free mode
                configure_pulseaudio($in);
                set_pulseaudio(1);
                set_pulseaudio_glitchfree(1);
                my $lib = get_libdir();
                $in->do_pkgs->ensure_is_installed($lib . 'alsa-plugins-pulseaudio',
                                                  '/usr/' . $lib . '/alsa-lib/libasound_module_pcm_pulse.so');
            } elsif ($audiosystem eq 'PipeWireWP') {
            #### PipeWire with WirePlumber
                configure_pipewire_wireplumber($in);
            } elsif ($audiosystem eq 'PipeWireMS') {
            #### PipeWire with PipeWire Media Session
                configure_pipewire_media_session($in);
            } else {
            #### Unmanaged value
                #TODO Error here
            }

            $in->ask_warn('', N("You need to reboot for changes to take effect")) if $::isStandalone;

        };

        my $warn_both = ($in->do_pkgs->is_installed('pipewire') && $in->do_pkgs->is_installed('pulseaudio') && (-e $::prefix . '/etc/systemd/user/sockets.target.wants/pipewire.socket' || -e $::prefix . '/etc/systemd/user/sockets.target.wants/pipewire.service')) ?
                    N("Warning: both pulseaudio and pipewire are installed and can conflict each other. Please fix your config by applying a choice") :
                    "";

        my $is_pipewire_available = $in->do_pkgs->is_available('task-pipewire');
        my $warn_pipewire_unavailable = !$is_pipewire_available ?
                    N("Warning: task-pipewire is not available in any media sources, so only pulseaudio could be set up. Please fix your repo configuration.") :
                    "";

        my @service_list = (
            'None',
            'PulseAudio',
            'PulseAudioGF',
        );
        if ($is_pipewire_available) {
            push @service_list, 'PipeWireWP';
            push @service_list, 'PipeWireMS';
        }
        my @common = (
            {
                label => N("Select the sound server"),
                title => 1,
            },
            {
                val => \$audiosystem,
                list => \@service_list,
                format => sub {
                    my ($choice) = @_;
                    +{
                        'None' => N("None"),
                        'PulseAudio' => N("PulseAudio"),
                        'PulseAudioGF' => N("PulseAudio with Glitch-Free mode"),
                        'PipeWireWP' => N("PipeWire with WirePlumber"),
                        'PipeWireMS' => N("PipeWire with PipeWire Media Session"),
                    }->{$choice};
                },
                type => 'list',
            },
            {
                advanced => 1,
                val => N("Reset sound mixer to default values"),
                clicked => sub { run_program::run('reset_sound') }
            },
            {
                val => N("Troubleshooting"), disabled => sub {},
                clicked => sub { &trouble($in) }
            },
        );

        my @messages = ("<b><i>" . $device->{description} . "</i></b>",
                        N("Your card uses the <b>\"%s\"</b> driver\n", $driver));

        if ($warn_both) {
            push @messages, ("<b>" . $warn_both . "</b>");
        }
        if (!$is_pipewire_available) {
            push @messages, "<b>" . $warn_pipewire_unavailable . "</b>";
        }
        if ($driver eq 'unknown') {
            if ($in->ask_from_({
                title => N("No alternative driver"),
                messages => N("There's no known OSS/ALSA alternative driver for your sound card (%s) which currently uses \"%s\"",
                              $device->{description}, $driver),
                          },
                          \@common,
                           )) {
                $write_config->();
            }
        } elsif ($in->ask_from_({ title => N("Sound configuration"),
                                  interactive_help_id => 'soundConfig',
                                  messages => \@messages
				},
                               \@common,
                                ))
        {
            $write_config->();
        }
    }
}

sub trouble {
    my ($in) = @_;
    $in->ask_warn(N("Sound troubleshooting"),
                  formatAlaTeX(
                               #-PO: keep the double empty lines between sections, this is formatted a la LaTeX
                               N("Below are some basic tips to help debug audio problems, but for accurate and up-to-date tips and tricks, please see:

https://wiki.mageia.org/en/Support:DebuggingSoundProblems



- General Recommendation: Enable PulseAudio. If you have opted to not to use PulseAudio, we would strongly advise you enable it. For the vast majority of desktop use cases, PulseAudio is the recommended and best supported option.



- \"kmix\" (KDE), \"gnome-control-center sound\" (GNOME) and \"pavucontrol\" (generic) will launch graphical applications to allow you to view your sound devices and adjust volume levels


- \"ps aux | grep pulseaudio\" will check that PulseAudio is running.


- \"pactl stat\" will check that you can connect to the PulseAudio daemon correctly.


- \"pactl list sink-inputs\" will tell you which programs are currently playing sound via PulseAudio.


- \"systemctl status osspd.service\" will tell you the current state of the OSS Proxy Daemon. This is used to enable sound from legacy applications which use the OSS sound API. You should install the \"ossp\" package if you need this functionality.


- \"pacmd ls\" will give you a LOT of debug information about the current state of your audio.


- \"lspcidrake -v | grep -i audio\" will tell you which low-level driver your card uses by default.


- \"/usr/sbin/lsmod | grep snd\" will enable you to check which sound related kernel modules (drivers) are loaded.


- \"alsamixer -c 0\" will give you a text-based mixer to the low level ALSA mixer controls for first sound card


- \"/usr/sbin/fuser -v /dev/snd/pcm* /dev/dsp\" will tell which programs are currently using the sound card directly (normally this should only show PulseAudio)
")));
}

sub configure_one_sound_slot {
    my ($modules_conf, $index, $driver) = @_;
    $modules_conf->set_sound_slot("sound-slot-$index", $driver);
    $modules_conf->set_options($driver, "xbox=1") if $driver eq "snd_intel8x0" && detect_devices::is_xbox();
    $modules_conf->set_options('snd-ac97-codec', "power_save=1") if $driver =~ /^snd/ && detect_devices::isLaptop();
}

sub configure_sound_slots {
    my ($modules_conf) = @_;
    my $altered = 0;
    each_index {
        my $default_driver = $modules_conf->get_alias("sound-slot-$::i");
        if (!member($default_driver, $_->{driver})) {
            $altered ||= $default_driver;
            configure_one_sound_slot($modules_conf, $::i, $_->{driver});
        }
    } detect_devices::getSoundDevices();
    $modules_conf->write if $altered && $::isStandalone;
}


1;
